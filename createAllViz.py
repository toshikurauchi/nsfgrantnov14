from createViz import createViz

for user_ID in range(1,3):
    for img_ID in range(1,13):
        print "Creating visualization for user " + str(user_ID) + " img " + str(img_ID)
        createViz(str(user_ID), str(img_ID))