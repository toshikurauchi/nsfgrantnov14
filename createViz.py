import numpy as np
import cv2
import sys
import csv
import math

FIXATION_THRESH = 10 # pixels
HEAT_RAD        = 200 # pixels
MIN_COLOR       = [0, 255, 0]
MAX_COLOR       = [0, 0, 255]

def createViz(user_ID, img_ID, transf=None, show_sp=False, show_hm=False, show_both=False):

    # Init file names
    img_file      = "data/imgs/"+img_ID+".png"
    prefix        = "data/"+user_ID+"/"+img_ID
    raw_data_file = prefix+".csv"
    offset_file   = prefix+"_offset.npy"
    scanpath_file = prefix+"_scan.png"
    heatmap_file  = prefix+"_heat.png"
    both_file     = prefix+"_scan_heat.png"
    weight_file   = prefix+"_weight_map.png"

    # Init transformations (offset and scale)
    if transf is not None:
        save_transf = True
    else:
        save_transf = False
        try:
            transf = np.load(offset_file)
        except:
            transf = (0, 0, 1, 1)

    # Read data
    data=[]
    ## world_frame_idx world_timestamp eye_timestamp x_norm y_norm x_scaled y_scaled on_srf
    with open(raw_data_file, 'rb') as csvfile:
        is_header = True
        spamreader = csv.reader(csvfile, delimiter='\t')
        for row in spamreader:
            if is_header:
                header = row
                is_header = False
            else:
                data.append([int(row[0]), float(row[1]), float(row[2]), float(row[3]), float(row[4]), float(row[5]), float(row[6]), row[7]=='True'])

    # Utility functions
    def denormalize(pt, shape, t): # t = (xOff, yOff, scaleX, scaleY)
        return ((pt[0]+t[0])*t[2]*shape[1],(1-pt[1]-t[1])*t[3]*shape[0])

    def add_to(img, sub, (x,y)):
        x = int(x)
        y = int(y)
        if x < 0 or x >= img.shape[1] or y < 0 or y >= img.shape[0]:
            return
        sub_h,sub_w = sub.shape
        sub_h2,sub_w2 = (sub_h-1)/2,(sub_w-1)/2
        img_h,img_w = img.shape
        l = min(sub_w2, x)
        r = min(sub_w2, img_w-x)
        t = min(sub_h2, y)
        b = min(sub_h2, img_h-y)
        sub_x,sub_y = sub_w2+1,sub_h2+1
        img[y-t:y+b,x-l:x+r] += sub[sub_y-t:sub_y+b,sub_x-l:sub_x+r]

    def draw_scanpath(sp, fixations, durations, thickness=3):
        for i in range(len(fixations)-1):
            f1,f2 = fixations[i],fixations[i+1]
            f1 = (int(f1[0]),int(f1[1]))
            f2 = (int(f2[0]),int(f2[1]))
            cv2.line(sp, f1, f2, (255, 0, 0), thickness)
        # Draw last fixation
        for i in range(len(fixations)):
            fix = (int(fixations[i][0]), int(fixations[i][1]))
            cv2.circle(sp, fix, int(durations[i]/0.5*30), (0, 0, 255), thickness)

    # Read image file
    img = cv2.imread(img_file)

    # Detect fixations
    fixations = []
    durations = []
    cur_fix   = []
    t0        = None
    gd = [denormalize((data[i][3],data[i][4]), img.shape, transf) for i in range(len(data))]
    for i in range(len(data)-1):
        g1,g2 = gd[i],gd[i+1]
        dif = (g2[0]-g1[0],g2[1]-g1[1])
        if math.sqrt(dif[0]*dif[0]+dif[1]*dif[1]) < FIXATION_THRESH:
            if t0 is None:
                t0 = data[i][1]
            cur_fix.append(g1)
        elif len(cur_fix) > 0:
            fixations.append(np.average(cur_fix, axis=0))
            durations.append(data[i][1]-t0)
            t0 = None
            cur_fix = []

    # Draw scanpath
    sp = img.copy()
    draw_scanpath(sp, fixations, durations)

    # Draw heatmap
    hm  = img.copy()
    # Create weight matrix
    wx,wy = np.mgrid[0.:2*HEAT_RAD+1,0.:2*HEAT_RAD+1]
    s   = HEAT_RAD*1/3
    m   = HEAT_RAD
    w   = 1/(2*math.pi*s*s)*np.exp(-0.5*(((wx-m)/s)*((wx-m)/s)+((wy-m)/s)*((wy-m)/s)))
    weights = np.zeros(shape=hm.shape[0:2])
    for i in range(len(gd)):
        add_to(weights, w, gd[i])
    weights /= np.max(weights)
    weights3 = np.empty(shape=(weights.shape[0],weights.shape[1],3))
    for i in range(3):
        weights3[:,:,i] = weights
    # Create mask
    h_mask = np.empty(shape=hm.shape)
    x,y = np.mgrid[0:hm.shape[1],0:hm.shape[0]]
    h_mask[y,x,:] = weights3[y,x]*MAX_COLOR + (1-weights3[y,x])*MIN_COLOR
    h_mask[weights3<0.005] = 0
    hm = ((1-np.power(weights3,0.8))*hm + np.power(weights3,0.8)*h_mask).astype(np.uint8)

    # Draw combined image
    comb = hm.copy()
    draw_scanpath(comb, fixations, durations)

    # Show visualizations
    if show_sp:
        cv2.imshow("Scanpath", sp)
    if show_hm:
        cv2.imshow("Heatmap", hm)
    if show_both:
        cv2.imshow("Gaze", comb)
    if show_sp or show_hm or show_both:
        cv2.waitKey(0)

    # Save files
    if save_transf:
        np.save(offset_file, transf)
    cv2.imwrite(scanpath_file, sp)
    cv2.imwrite(heatmap_file, hm)
    cv2.imwrite(both_file, comb)
    cv2.imwrite(weight_file, weights*255)

if __name__=="__main__":
    if len(sys.argv) < 3:
        print "USAGE: python " + sys.argv[0] + " userID imgID [xOff yOff scaleX scaleY]"
        sys.exit(0)

    # Read command line args
    user_ID = sys.argv[1]
    img_ID  = sys.argv[2]

    transf = None
    if len(sys.argv) >= 7:
        transf = (float(sys.argv[3]),float(sys.argv[4]), float(sys.argv[5]), float(sys.argv[6]))

    show_sp,show_hm,show_both = False,False,False

    createViz(user_ID, img_ID, transf, show_sp, show_hm, True)